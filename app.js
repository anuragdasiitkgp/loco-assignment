// Require libraries and imports
import getSequelizeInstance from './db.js';
import transaction from './models/transaction.js';
import { findByIdController, findByTypeController, createOrUpdateTransaction, sumNodes } from './controllers/transaction.controller.js';
import express from 'express';
import bodyParser from 'body-parser';

// Initializations
const app = express();

// Sync sequelize
let sequelize = getSequelizeInstance();
sequelize.sync();

// Middlewares
app.use(bodyParser.json());

// Routers
app.get('/transactionservice/transaction/:tid', findByIdController);
app.get('/transactionservice/types/:type', findByTypeController);
app.put('/transactionservice/transaction/:tid', createOrUpdateTransaction);
app.get('/transactionservice/sum/:tid', sumNodes);

export default app;