export default {
  SOMETHING_WENT_WRONG: "Something went wrong!",
  NO_RECORDS_FOUND: "No entries were found for your query",
  NO_PARENT_FOUND: "No parent was found for given parent id",
  INVALID_ID: "Invalid Id",
  SUCCESS: "Success",
  OK: "ok"
};