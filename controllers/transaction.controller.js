import { findTransactions, createTransaction, updateTransaction, getNodeSum } from '../services/transaction.service.js';
import constants from '../constants.js';

export async function findByIdController(req, res) {
  try {
    if(!req.params.tid) { return res.status(500).json({message: constants.INVALID_ID}); }
    let results = await findTransactions({id: parseInt(req.params.tid)});
    if(results && results.length > 0) {
      return res.status(200).json(results[0]);
    }
    return res.status(404).json({message: constants.NO_RECORDS_FOUND});
  } catch(e) {
    return res.status(500).json({message: constants.SOMETHING_WENT_WRONG});
  }
}

export async function findByTypeController(req, res) {
  try {
    if(!req.params.type) { return res.status(500).json({message: constants.INVALID_ID}); }
    let transactions = await findTransactions({type: req.params.type});
    if(transactions && transactions.length > 0) {
      console.log(transactions)
      let results = transactions.map(t => t.id);
      return res.status(200).json(results);
    }
    return res.status(404).json({message: constants.NO_RECORDS_FOUND});
  } catch(e) {
    return res.status(500).json({message: constants.SOMETHING_WENT_WRONG});
  }
}

export async function createOrUpdateTransaction(req, res) {
  try {
    if(!req.params.tid) { return res.status(500).json({message: constants.INVALID_ID}); }
    let results = await findTransactions({id: parseInt(req.params.tid)});
    let body = req.body;
    if(body.parentId != null || body.parentId != undefined) {
      let parent = await findTransactions({id: parseInt(body.parentId)});
      if(!parent || (parent && parent.length == 0)) {
        return res.status(400).json({message: constants.NO_PARENT_FOUND});
      }
    }
    if(results && results.length > 0) {
      let result = await updateTransaction({id: req.params.tid}, body);
      if(result) {
        return res.status(201).json({status: constants.OK});
      } else {
        return res.status(500).json({message: constants.SOMETHING_WENT_WRONG});
      }
    }
    let result = await createTransaction({...body, id: req.params.tid});
    if(result) {
      return res.status(200).json({status: constants.OK});
    } else {
      return res.status(500).json({message: constants.SOMETHING_WENT_WRONG});
    }
  } catch(e) {
    console.log(e)
    return res.status(500).json({message: constants.SOMETHING_WENT_WRONG});
  }
}

export async function sumNodes(req, res) {
  try {
    if(!req.params.tid) { return res.status(500).json({message: constants.INVALID_ID}); }
    let result = await getNodeSum(parseInt(req.params.tid));
    if(result && result.length > 0 && result[0]['total_amount'] != null) {
      return res.status(200).json({sum: result[0]['total_amount']});
    }
    return res.status(404).json({message: constants.NO_RECORDS_FOUND});
  } catch(e) {
    return res.status(500).json({message: constants.SOMETHING_WENT_WRONG});
  }
}