import { Sequelize } from 'sequelize';
import dotenv from 'dotenv';

let dbInstance = null;
let getSequelizeInstance = () => {
  if(dbInstance) return dbInstance;
  dotenv.config();
  dbInstance = new Sequelize(process.env.DB_CONNECTION_STRING);
  return dbInstance;
};

export default getSequelizeInstance;