import { DataTypes } from 'sequelize';
import getSequelizeInstance from '../db.js';

let sequelize = getSequelizeInstance();
let Transaction = sequelize.define('transaction', {
  id: {
    type: DataTypes.BIGINT,
    allowNull: false,
    primaryKey: true
  },
  type: {
    field: "transaction_type",
    type: DataTypes.STRING,
    allowNull: false
  },
  amount: {
    type: DataTypes.DOUBLE,
    allowNull: false
  },
  parentId: {
    field: "parent_id",
    type: DataTypes.BIGINT
  }
});

// Relationship
Transaction.hasMany(Transaction, {as: 'children', foreignKey: 'parentId'});
Transaction.belongsTo(Transaction, {as: 'parent', foreignKey: 'parentId'});

export default Transaction;