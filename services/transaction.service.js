import getSequelizeInstance from '../db.js';
import { QueryTypes } from 'sequelize';
import Transaction from '../models/transaction.js';

export async function findTransactions(query) {
  try {
    let result = await Transaction.findAll({where: query});
    return result;
  } catch(e) {
    console.error(e);
  }
}

export async function createTransaction(params) {
  try {
    await Transaction.create(params);
    return true;
  } catch(e) {
    console.error(e);
  }
  return false;
}

export async function updateTransaction(query, params) {
  try {
    await Transaction.update(params, {where: query});
    return true;
  } catch(e) {
    console.error(e);
  }
  return false;
}

export async function getNodeSum(nodeId) {
  try {
    let query = `
      WITH RECURSIVE cte AS (
        SELECT * FROM transactions where id=?
        UNION ALL
        SELECT t.* FROM transactions t JOIN cte ON t.parent_id = cte.id
      )
      SELECT SUM(amount) AS total_amount FROM CTE 
    `;
    let sequelize = getSequelizeInstance();
    let result = await sequelize.query(query, {replacements: [nodeId], type: QueryTypes.SELECT});
    return result;
  } catch(e) {
    console.error(e);
  }
  return false;
}
